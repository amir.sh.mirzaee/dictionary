package mirzaee.amir.dictionary.web;

import mirzaee.amir.dictionary.models.WordMoled;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {


@GET("search?token=52919.srgFIZ6cwGxEMO2v5oV0IsFpjUsbEug7WwNEwTR3")
    Call<WordMoled>getRestWord(@Query(value = "q",encoded = true)String question,@Query(value = "type")String typ,@Query("filter")String fitter);
}
