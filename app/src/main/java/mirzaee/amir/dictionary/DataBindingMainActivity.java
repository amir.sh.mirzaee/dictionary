package mirzaee.amir.dictionary;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import mirzaee.amir.dictionary.adapters.RecycleAdapter;
import mirzaee.amir.dictionary.databinding.ActivityDataBindingMainBinding;
import mirzaee.amir.dictionary.models.Result;
import mirzaee.amir.dictionary.models.WordMoled;
import mirzaee.amir.dictionary.web.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataBindingMainActivity extends AppCompatActivity implements EvetnInterface {
//    private RecyclerView resultList;
    EditText word;
    ImageView sarech;
    RecycleAdapter adapter;

   private ActivityDataBindingMainBinding activityDataBindingMainBinding;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_data_binding_main);
        activityDataBindingMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_data_binding_main);
        activityDataBindingMainBinding.resultList.setLayoutManager(new LinearLayoutManager(this));
        activityDataBindingMainBinding.setHandler(this);


        init();
    }

    private void init() {
//        resultList = findViewById(R.id.result_list);
        sarech = findViewById(R.id.search);
        word = findViewById(R.id.word);

//        resultList.setLayoutManager(new LinearLayoutManager(this));

        sarech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (word.length() > 0)
//                    getResult(word.getText().toString());
            }
        });
    }

    public void getResult(String word) {
        ApiInterface apiInterface = ((MyApplication) getApplication()).getRetrofitClient();
        Call<WordMoled> call = apiInterface.getRestWord(word, "exact", "dehkhoda,moein,amid");
        call.enqueue(new Callback<WordMoled>() {
            @Override
            public void onResponse(Call<WordMoled> call, Response<WordMoled> response) {

                if (response != null) {
                    adapter = new RecycleAdapter(DataBindingMainActivity.this, response.body().getData().getResults(), DataBindingMainActivity.this);
//                    resultList.setAdapter(adapter);
                    activityDataBindingMainBinding.resultList.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<WordMoled> call, Throwable t) {
                Toast.makeText(DataBindingMainActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void shearInterface(Result r) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, r.getText());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void copyInterface(Result result) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("text", result.getText());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, "Text Copied",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void searchClick() {
        if (word.length() > 0)
            getResult(word.getText().toString());
    }
}
