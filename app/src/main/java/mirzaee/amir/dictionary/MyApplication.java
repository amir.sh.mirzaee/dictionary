package mirzaee.amir.dictionary;

import android.app.Application;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import mirzaee.amir.dictionary.web.ApiInterface;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyApplication extends Application {

    ApiInterface client;
    Retrofit retrofit;
    private static final String BASE_URL = "http://api.vajehyab.com/v3/";


    @Override
    public void onCreate() {
        super.onCreate();

        client=getClient().create(ApiInterface.class);
    }


    private synchronized Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient())
                    .build();
        }
        return retrofit;
    }

    private OkHttpClient okHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        return chain.proceed(chain.request()
                                .newBuilder()
                                .build()
                        );
                    }
                })
                .build();
    }

    public ApiInterface getRetrofitClient(){
        return client;
    }
}
