
package mirzaee.amir.dictionary.model2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SugestList {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("data")
    @Expose
    private Data data;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
