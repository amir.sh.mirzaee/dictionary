package mirzaee.amir.dictionary;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import mirzaee.amir.dictionary.adapters.WordAdapter;
import mirzaee.amir.dictionary.model2.SugestList;
import mirzaee.amir.dictionary.models.WordMoled;

public class Main2Activity extends AppCompatActivity {
    ListView list;
    EditText word;
    ImageView sarech,shear,copy;
    ViewGroup viewGroup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
//        View rowView = LayoutInflater.from(this).inflate(R.layout.list_item,viewGroup,false);

        list=(ListView)findViewById(R.id.listitem);
        word=(EditText) findViewById(R.id.word);
        sarech=(ImageView)findViewById(R.id.search);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action);
        sarech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (word.length() > 0)
                    getresult(word.getText().toString());
//                View view1 = this.getCurrentFocus();
//                if (view1 != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                }
            }
        });

        word.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (word.length() > 0)
                        getresult(word.getText().toString());
                    View view = getCurrentFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });


//        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                ImageView copy =(ImageView)view.findViewById(R.id.copys);
//
//            }
//        });

//        ArrayAdapter<String> adapter = new
//                ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,sug(word.getText().toString()));
//            AutoAdapter adapter = new AutoAdapter(this, android.R.layout.simple_dropdown_item_1line);
//            word.setAdapter(adapter);
    }
    public List<String> sug(String valu){
        final List<String> sugarrya=new ArrayList<>();
        String sgurl="http://api.vajehyab.com/v3/suggest?token=52919.oM3dueTTQxe506IQwq1pB5J6G0Sbm9jLVmYpBSjs&q="+valu;
        AsyncHttpClient client=new AsyncHttpClient();
        client.get(sgurl, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(Main2Activity.this, (CharSequence) throwable, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                setarry(responseString);
                Gson gson = new Gson();
                SugestList moled = gson.fromJson(responseString, SugestList.class);
                if (word.length() > 0) {
                    int lgh = moled.getData().getSuggestion().size();
//                List sugarrya=new ArrayList();
                    for (int i = 0; i >= lgh; i++) {
                        sugarrya.add(moled.getData().getSuggestion().get(i));
                    }
                }
            }
        });
        return sugarrya;
    }
    public void getresult(String valu) {
    String urltext = "http://api.vajehyab.com/v3/search?token=52919.srgFIZ6cwGxEMO2v5oV0IsFpjUsbEug7WwNEwTR3&q="
               +valu+"&type=exact&filter=dehkhoda,moein,amid";
        //String urltext = "http://api.vajehyab.com/v3/search?token=52919.srgFIZ6cwGxEMO2v5oV0IsFpjUsbEug7WwNEwTR3&q="
              //  +valu+"&type=exact&filter=dehkhoda";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(urltext, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
               Toast.makeText(Main2Activity.this, throwable.toString(), Toast.LENGTH_LONG).show();
               //Log.d("respons",responseString.toString()+"throwable "+throwable.toString()+"cod "+String.valueOf(statusCode));

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                settext(responseString);
            }
        });
//    public void setarry (String resulttext){
//        Gson gson=new Gson();
//        SugestList moled = gson.fromJson(resulttext,SugestList.class);
//        int lgh= moled.getData().getSuggestion().size();
//        List sugarrya=new ArrayList();
//        for (int i=0;i>=lgh;i++){
//            sugarrya.add(moled.getData().getSuggestion().get(1));
//        }
//
//
//
//    }
    }
    public void settext (String resulttext){
        List<String> titel=new ArrayList<>();
        List<String> result=new ArrayList<>();
        Gson gson=new Gson();
        WordMoled moled = gson.fromJson(resulttext,WordMoled.class);
        int lenR =moled.getData().getResults().size();
        for (int i=0;i<lenR;i++){
            result.add(moled.getData().getResults().get(i).getText());
        }

        for (int i=0;i<lenR;i++){
            titel.add(moled.getData().getResults().get(i).getSource());
        }
        WordAdapter adapter =new WordAdapter(Main2Activity.this,titel,result,word.getText().toString());
        list.setAdapter(adapter);

    }
}
