package mirzaee.amir.dictionary;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import mirzaee.amir.dictionary.models.WordMoled;

public class MainActivity extends AppCompatActivity {

    EditText qustion;
    TextView result;
    Button show;
    ImageView copy , share;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        qustion=(EditText)findViewById(R.id.inserttext);
        result=(TextView)findViewById(R.id.result);
        show=(Button)findViewById(R.id.get);
        copy=(ImageView)findViewById(R.id.cop);
        share=(ImageView)findViewById(R.id.share);

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String valu=qustion.getText().toString();
                getresult(valu);
            }
        });
    }
    public void getresult(String valu){
        String urltext="http://api.vajehyab.com/v3/search?token=52919.oM3dueTTQxe506IQwq1pB5J6G0Sbm9jLVmYpBSjs&q="+valu+"&type=exact&filter=dehkhoda,moein,amid";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(urltext, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(MainActivity.this, (CharSequence) throwable, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                settext(responseString);
            }
        });

        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("text", result.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(MainActivity.this, "Text Copied",
                        Toast.LENGTH_SHORT).show();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, result.getText().toString());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
    }
    public void settext (String resulttext){
        Gson gson=new Gson();
        WordMoled moled = gson.fromJson(resulttext,WordMoled.class);
        String rr=moled.getData().getResults().get(1).getText();


        result.setText(rr);

    }
}
