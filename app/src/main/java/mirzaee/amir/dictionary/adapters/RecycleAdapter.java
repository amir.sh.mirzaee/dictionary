package mirzaee.amir.dictionary.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

import mirzaee.amir.dictionary.EvetnInterface;
import mirzaee.amir.dictionary.R;
import mirzaee.amir.dictionary.databinding.ListItemDataBindBinding;
import mirzaee.amir.dictionary.models.Result;

public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.MyViewHolder>  {





    Context context;
    List<Result> results = new ArrayList<>();
    public ViewDataBinding binding;
    private final EvetnInterface listener;

    public RecycleAdapter(Context context, List<Result> results, EvetnInterface listener) {
        this.context = context;
        this.results = results;

        this.listener = listener;
    }

    @NonNull
    @Override
    public RecycleAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ListItemDataBindBinding lBinding=DataBindingUtil.inflate(
                layoutInflater,R.layout.list_item_data_bind, viewGroup, false);
        return new MyViewHolder(lBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Result result= results.get(i);
        myViewHolder.listItemDataBindBinding.setResult(result);



        myViewHolder.bind(result , listener);
        Log.d("click", "" + myViewHolder.getItemId());
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

 /*   @Override
    public void shearInterface(Result r) {
        Result ee=r;
    }*/

    class MyViewHolder extends RecyclerView.ViewHolder {

        ListItemDataBindBinding listItemDataBindBinding;

        public MyViewHolder(@NonNull ListItemDataBindBinding itemView) {
            super(itemView.getRoot());

            listItemDataBindBinding = itemView;
//            context = itemView.getRoot().getContext();

        }

        public void bind(final Result obj , final EvetnInterface listener) {

            listItemDataBindBinding.shares.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.shearInterface(obj);
                }
            });
            listItemDataBindBinding.copi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.copyInterface(obj);
                }
            });
        }

/*
        public void shearText(Result rlx) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, rl.getText());
            sendIntent.setType("text/plain");
            context.startActivity(sendIntent);
        }*/
    }
}
