package mirzaee.amir.dictionary.adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import mirzaee.amir.dictionary.R;

/**
 * Created by amir on 26/11/2017.
 */

public class WordAdapter extends BaseAdapter {
    Context context;
    List sors;
    List resulte;
    String word;

    public WordAdapter() {
    }

    public WordAdapter(Context context, List sors, List result, String word) {
        this.context = context;
        this.sors = sors;
        this.resulte = result;
        this.word = word;
    }

    @Override
    public int getCount() {
        return sors.size();
    }

    @Override
    public Object getItem(int i) {
        return sors.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.list_item,viewGroup,false);
        mirzaee.amir.dictionary.views.MyTextView sorse= rowView.findViewById(R.id.type);
        mirzaee.amir.dictionary.views.MyTextView result=rowView.findViewById(R.id.result);
        mirzaee.amir.dictionary.views.MyTextView titel= rowView.findViewById(R.id.titel);
        ImageView copy=rowView.findViewById(R.id.copys);
        ImageView share=rowView.findViewById(R.id.shares);
        sorse.setText(sors.get(i).toString());
        result.setText(resulte.get(i).toString());
        titel.setText(word);

        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("text", resulte.get(i).toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(context, "Text Copied",
                        Toast.LENGTH_SHORT).show();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, resulte.get(i).toString());
                sendIntent.setType("text/plain");
                context.startActivity(sendIntent);
            }
        });

        return rowView;
    }
}
