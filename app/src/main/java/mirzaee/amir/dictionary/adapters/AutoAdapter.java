//package mirzaee.amir.dictionary.adapters;
//
//import android.app.Notification;
//import android.content.Context;
//import android.util.Log;
//import android.widget.ArrayAdapter;
//import android.widget.Filter;
//import android.widget.Filterable;
//
//import java.util.ArrayList;
//
///**
// * Created by amir on 25/11/2017.
// */
//
//public class AutoAdapter extends ArrayAdapter<Notification.Style> implements Filterable {
//    private ArrayList<Notification.Style> mData;
//
//    public AutoAdapter(Context context, int textViewResourceId) {
//        super(context, textViewResourceId);
//        mData = new ArrayList<Notification.Style>();
//    }
//
//    @Override
//    public int getCount() {
//        return mData.size();
//    }
//
//    @Override
//    public Notification.Style getItem(int index) {
//        return mData.get(index);
//    }
//
//    @Override
//    public Filter getFilter() {
//        Filter myFilter = new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence constraint) {
//                FilterResults filterResults = new FilterResults();
//                if(constraint != null) {
//                    // A class that queries a web API, parses the data and returns an ArrayList<Style>
//                    StyleFetcher fetcher = new StyleFetcher();
//                    try {
//                        mData = fetcher.retrieveResults(constraint.toString());
//                    }
//                    catch(Exception e) {
//                        Log.e("myException", e.getMessage());
//                    }
//                    // Now assign the values and count to the FilterResults object
//                    filterResults.values = mData;
//                    filterResults.count = mData.size();
//                }
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence contraint, FilterResults results) {
//                if(results != null && results.count > 0) {
//                    notifyDataSetChanged();
//                }
//                else {
//                    notifyDataSetInvalidated();
//                }
//            }
//        };
//        return myFilter;
//    }
//}